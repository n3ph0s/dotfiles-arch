return {
  {
    'vhyrro/luarocks.nvim',
    priority = 1001,
    opts = {
      rocks = { 'magick' },
    },
  },
  {
    '3rd/image.nvim',
    dependencies = { 'luarocks.nvim' },
    config = function()
      local function file_exists(path)
        local f = io.open(path, 'r')
        if f then
          f:close()
          return true
        else
          return false
        end
      end

      require('image').setup {
        -- backend = 'kitty',
        -- kitty_method = 'normal',
        backend = 'ueberzug',
        integrations = {
          markdown = {
            enabled = true,
            clear_in_insert_mode = false,
            download_remote_images = true,
            only_render_image_at_cursor = true,
            filetypes = { 'markdown', 'vimwiki' },
            resolve_image_path = function(document_path, image_path, fallback)
              local vault_root = os.getenv 'HOME' .. '/Vaults/Master_Vault/'

              -- Check if it's an absolute path
              if image_path:sub(1, 1) == '/' then
                if file_exists(image_path) then
                  return image_path
                end
              end

              -- Check if it's a path relative to the document
              local document_dir = vim.fn.fnamemodify(document_path, ':h')
              local relative_to_doc = document_dir .. '/' .. image_path
              if file_exists(relative_to_doc) then
                return relative_to_doc
              end

              -- Try Obsidian-style path
              local obsidian_path = vault_root .. image_path
              if file_exists(obsidian_path) then
                return obsidian_path
              end

              -- If all else fails, use the fallback
              return fallback(document_path, image_path)
            end,
          },
        },
        max_width = 100,
        max_height = 20,
        max_width_window_percentage = 70,
        max_height_window_percentage = 35,
        window_overlap_clear_enabled = true,
        window_overlap_clear_ft_ignore = { 'cmp_menu', 'cmp_docs', '' },
        editor_only_render_when_focused = false,
        tmux_show_only_in_active_window = true,
        hijack_file_patterns = { '*.png', '*.jpg', '*.jpeg', '*.gif', '*.webp', '*.avif' },
      }
    end,
  },
}
