return {
  'epwalsh/obsidian.nvim',
  version = '*',
  lazy = false,
  ft = 'markdown',

  dependencies = {
    -- Required.
    'nvim-lua/plenary.nvim',
  },

  keys = {
    { '<leader>on', '<cmd>ObsidianNew<cr>', desc = 'Create a New Obsidian Document' },
    { '<leader>op', '<cmd>execute "ObsidianPasteImg ".strftime("%Y%m%d%H%M%S")<cr>', desc = 'Obsidian Paste Image' },
    { '<leader>or', '<cmd>ObsidianRename<cr>', desc = 'Obsidian Rename current note' },
    { '<leader>ot', '<cmd>ObsidianTemplate<cr>', desc = 'Insert Obsidian Template into current note' },
    { '<leader>of', '<cmd>ObsidianFollowLink<cr>', desc = 'Obsidian Follow Link' },
    { '<leader>ox', '<cmd>ObsidianExtractNote<cr>', desc = 'Obsidian Extract Selection to New Note' },
    { '<leader>oo', '<cmd>ObsidianOpen<cr>', desc = 'Open current Note in Obsidian Application' },
    { '<leader>ol', '<cmd>ObsidianLinkNew<cr>', desc = 'Obsidian New Linked File from Visual Selection' },
  },

  opts = {
    workspaces = {
      {
        name = 'master',
        path = vim.fn.expand '~/Vaults/Master_Vault/',
      },
    },

    notes_subdir = '_Inbox',

    ui = { enable = false },
    disable_frontmatter = true,
    templates = {
      subdir = '99.Meta/templates',
      date_format = '%Y-%m-%d',
      time_format = '%I:%M',
      tags = '',
    },

    attachments = {
      img_folder = '99.Meta/assets',
    },

    mappings = {},

    note_id_func = function(title)
      return title
    end,
  },
}
