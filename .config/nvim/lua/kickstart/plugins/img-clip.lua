local function get_image_dir()
  local current_dir = vim.fn.expand '%:p:h'
  local assets_dir = current_dir .. '/assets'
  vim.fn.mkdir(assets_dir, 'p')
  return 'assets' -- Return relative path
end

return {
  'HakonHarnes/img-clip.nvim',
  event = 'VeryLazy',
  opts = {
    default = {
      dir_path = get_image_dir,
      file_name = function()
        return os.date '%Y%m%d%H%M%S'
      end,
      url_encode_path = false,
      relative_to_current_file = true,
      prompt_for_file_name = false,
      show_dir_path_in_prompt = false,
      use_absolute_path = false,
    },
    filetypes = {
      markdown = {
        url_encode_path = true,
        template = '![$CURSOR]($FILE_PATH)',
      },
    },
  },
  keys = {
    { '<leader>p', '<cmd>PasteImage<cr>', desc = 'Paste image from system clipboard' },
  },
}
