import os
import subprocess
import themes # Custom Color Theme
from libqtile import bar, hook, layout, qtile, widget
from libqtile.config import Click, Drag, DropDown, Group, Key, Match, ScratchPad, Screen
from libqtile.lazy import lazy
# from libqtile.utils import guess_terminal
# Import qtile-extras
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration

mod = "mod4"
terminal = "alacritty -e tmux"
screenlock = "slock"

#Call Autostart
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/autostart.sh"])

# Function to hide / show all the windows in a group
@lazy.function
def minimize_all(qtile):
    for win in qtile.current_group.windows:
        if hasattr(win, "toggle_minimize"):
            win.toggle_minimize()

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Launch Applications
    Key([mod], "d", lazy.spawn("rofi -show drun"), desc="Run Launcher"),
    Key([mod], "b", lazy.spawn("brave --profile-directory='Default'"), lazy.group["3"].toscreen(), desc="Launch Brave Browser - Work"),
    Key([mod, "shift"], "b", lazy.spawn("brave --profile-directory='Profile 2'"), lazy.group["6"].toscreen(), desc="Launch Brave Browser - Personal"),
    Key([mod, "shift"], "x", lazy.spawn(screenlock), desc="Screen Lock"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key(["control", "shift"], "a", lazy.spawn("flameshot gui"), desc="Screenshot"),

    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.reset(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key( [mod, "shift"], "Return", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack",),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen on the focused window",),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),
    Key([mod], "m", lazy.layout.maximize(), desc="toggle between min and max sizes"),
    Key([mod, "shift"], "m", minimize_all(), desc="Toggle hide/show all windows on current group"),
    Key([mod, "shift"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
]

# Add key bindings to switch VTs in Wayland.
# We can't check qtile.core.name in default config as it is loaded before qtile is started
# We therefore defer the check until the key binding is run by using .when(func=...)
for vt in range(1, 8):
    keys.append(
        Key(
            ["control", "mod1"],
            f"f{vt}",
            lazy.core.change_vt(vt).when(func=lambda: qtile.core.name == "wayland"),
            desc=f"Switch to VT{vt}",
        )
    )

# groups = [Group(i) for i in "123456789"]
groups = [
    Group("1", label="1", layout="monadtall"),
    Group("2", label="2", matches=[Match(wm_class="signal"),Match(wm_class="discord")], layout="monadtall"),
    Group("3", label="3", layout="monadtall"),
    Group("4", label="4", matches=[Match(wm_class="obsidian")], layout="monadtall"),
    Group("5", label="5", layout="monadtall"),
    Group("6", label="6", layout="monadtall"),
    Group("7", label="7", matches=[Match(wm_class="virt-manager")], layout="monadtall"),
    Group("8", label="8", layout="monadtall"),
    Group("9", label="9", layout="monadtall"),
]

for i in groups:
    keys.extend(
        [
            # mod + group number = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod + shift + group number = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod + shift + group number = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

# Append scratchpad with dropdowns to groups
groups.append(ScratchPad("scratchpad", [
    DropDown("term", "alacritty", width=0.5, height=0.6, x=0.3, y=0.2),
    DropDown("ranger", "alacritty -e ranger", width=0.5, height=0.6, x=0.3, y=0.2),
    DropDown("mixer", "pavucontrol", width=0.5, height=0.6, x=0.3, y=0.2),
    DropDown("proton", "proton-pass", width=0.5, height=0.6, x=0.3, y=0.2),
]))

# Extend keys list with key bindings for scrathpad
keys.extend([
    Key([], "F1", lazy.group["scratchpad"].dropdown_toggle("term")),
    Key([], "F2", lazy.group["scratchpad"].dropdown_toggle("ranger")),
    Key([], "F3", lazy.group["scratchpad"].dropdown_toggle("mixer")),
    Key([], "F4", lazy.group["scratchpad"].dropdown_toggle("proton")),
])

# Define Color Theme
colors = themes.DoomOne

layout_theme = {
    "border_width" : 3,
    "margin" : 8,
    "border_focus" : colors[8],
    "border_normal" : colors[0]
}

layouts = [
    # layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    layout.Max(
        border_width = 0,
        margin = 0
    ),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    layout.MonadTall(**layout_theme),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(
    #     font = "Fira Code Nerd Font",
    #     fontsize = 11,
    #     border_width = 0,
    #     bg_color = colors[0],
    #     active_bg = colors[8],
    #     active_fg = colors[2],
    #     inactive_bg = colors[1],
    #     inactive_fg = colors[0],
    #     padding_left = 8,
    #     padding_x = 8,
    #     padding_y = 6,
    #     sections = ["ONE", "TWO", "THREE"],
    #     section_fontsize = 10,
    #     section_fg = colors[7],
    #     section_top = 15,
    #     section_bottom = 15,
    #     level_shift = 8,
    #     vspace = 3,
    #     panel_width = 240
    # ),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="Fira Code Nerd Font",
    fontsize=12,
    padding=3,
    background=colors[0]
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Prompt(
                    font = "Fira Code Nerd Font",
                    fontsize = 14,
                    foreground = colors[1]
                ),
                widget.GroupBox(
                    fontsize = 14,
                    margin_y = 5,
                    margin_x = 5,
                    padding_y = 0,
                    padding_x = 1,
                    borderwidth = 3,
                    active = colors[8],
                    inactive = colors[1],
                    rounded = False,
                    highlight_color = colors[2],
                    highlight_method = "line",
                    this_current_screen_border = colors[7],
                    this_screen_border = colors [4],
                    other_current_screen_border = colors[7],
                    other_screen_border = colors[4]
                ),
                widget.TextBox(
                    text = "|",
                    font = "Fira Code Nerd Font",
                    foreground = colors[1],
                    padding = 2,
                    fontsize = 14
                ),
                widget.CurrentLayoutIcon(
                    foreground = colors[1],
                    padding = 2,
                    fontsize = 14
                ),
                widget.CurrentLayout(
                    foreground = colors[1],
                    padding = 5,
                    fontsize = 14
                ),
                widget.TextBox(
                    text = "|",
                    font = "Fira Code Nerd Font",
                    foreground = colors[1],
                    padding = 2,
                    fontsize = 14
                ),
                widget.WindowName(
                    foreground = colors[1],
                    max_chars = 40,
                    fontsize = 14
                ), 
                widget.Spacer(length = 8),
                widget.CPU(
                    format = '▓ CPU: {load_percent}%',
                    foreground = colors[4],
                    fontsize = 14,
                    decorations=[
                        BorderDecoration(
                            colour = colors[4],
                            border_width = [0, 0, 2, 0],
                        )
                    ], 
                ),
                widget.Spacer(length = 8),
                widget.Memory(
                    foreground = colors[8],
                    fontsize = 14,
                    format = '{MemUsed: .0f}{mm}',
                    fmt = '🖥 Mem: {} used',
                    decorations=[
                        BorderDecoration(
                            colour = colors[8],
                            border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
                widget.Spacer(length = 8),
                widget.DF(
                    update_interval = 60,
                    foreground = colors[5],
                    fontsize = 14,
                    partition = '/home',
                    #format = '[{p}] {uf}{m} ({r:.0f}%)',
                    format = '{uf}{m} free',
                    fmt = '🖴  Disk: {}',
                    visible_on_warn = False,
                    decorations=[
                        BorderDecoration(
                            colour = colors[5],
                            border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
                widget.Spacer(length = 8),
                widget.Volume(
                    foreground = colors[7],
                    fontsize = 14,
                    fmt = '🕫  Vol: {}',
                    decorations=[
                        BorderDecoration(
                            colour = colors[7],
                            border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
                widget.Spacer(length = 8),
                widget.Wallpaper(
                    directory = "~/wallpapers/",
                    label = "Wallpaper",
                    random_selection = True,
                    foreground = colors[6],
                    fontsize = 14,
                    random_Selection = True,
                    wallpaper_command = [
                        "feh", "--bg-fill", "--no-fehbg"
                    ],
                    decorations = [
                        BorderDecoration(
                            colour = colors[6],
                            border_width = [0, 0, 2, 0],
                        )
                    ]
                ),
                widget.Clock(
                    foreground = colors[8],
                    fontsize = 14,
                    format = "⏱  %a, %d %b - %H:%M",
                    decorations=[
                        BorderDecoration(
                            colour = colors[8],
                            border_width = [0, 0, 2, 0],
                        )
                    ],
                ),
                # widget.Systray(padding = 3),
                widget.Spacer(length = 8),
            ],
            24,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
        # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        # x11_drag_polling_rate = 60,
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(wm_class="pavucontrol"),  # Volume Control
        Match(wm_class="proton pass"),  # Proton Pass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# xcursor theme (string or None) and size (integer) for Wayland backend
wl_xcursor_theme = None
wl_xcursor_size = 24

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
